using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProtoBuf
{
    internal sealed class StaticBufferPool
    {
        private const int POOL_SIZE = 20;
        internal const int BUFFER_LENGTH = 1024;

        private StaticBufferPool() { }
        
        private static readonly CachedBuffer[] Pool = new CachedBuffer[POOL_SIZE];

        internal static void Flush()
        {
            lock (Pool)
            {
                for (var i = 0; i < Pool.Length; i++)
                    Pool[i] = null;
            }
        }

        internal static byte[] GetBuffer() => GetBuffer(BUFFER_LENGTH);
        internal static byte[] GetBuffer(int minSize)
        {
            byte[] cachedBuff = GetCachedBuffer(minSize);
            return cachedBuff ?? new byte[minSize];
        }
        internal static byte[] GetCachedBuffer(int minSize)
        {
            lock (Pool)
            {
                var bestIndex = -1;
                byte[] bestMatch = null;
                for (var i = 0; i < Pool.Length; i++)
                {
                    var buffer = Pool[i];
                    if (buffer == null || buffer.Size < minSize)
                    {
                        continue;
                    }
                    if (bestMatch != null && bestMatch.Length < buffer.Size)
                    {
                        continue;
                    }

                    var tmp = buffer.Buffer;
                    if (tmp == null)
                    {
                        Pool[i] = null;
                    }
                    else
                    {
                        bestMatch = tmp;
                        bestIndex = i;
                    }
                }

                if (bestIndex >= 0)
                {
                    Pool[bestIndex] = null;
                }

                return bestMatch;
            }
        }

        /// <remarks>
        /// https://docs.microsoft.com/en-us/dotnet/framework/configure-apps/file-schema/runtime/gcallowverylargeobjects-element
        /// </remarks>
        private const int MaxByteArraySize = int.MaxValue - 56;

        internal static void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
        {
            Helpers.DebugAssert(buffer != null);
            Helpers.DebugAssert(toFitAtLeastBytes > buffer.Length);
            Helpers.DebugAssert(copyFromIndex >= 0);
            Helpers.DebugAssert(copyBytes >= 0);

            int newLength = buffer.Length * 2;
            if (newLength < 0)
            {
                newLength = MaxByteArraySize;
            }

            if (newLength < toFitAtLeastBytes) newLength = toFitAtLeastBytes;

            if (copyBytes == 0)
            {
                ReleaseBufferToPool(ref buffer);
            }

            var newBuffer = GetCachedBuffer(toFitAtLeastBytes) ?? new byte[newLength];

            if (copyBytes > 0)
            {
                Buffer.BlockCopy(buffer, copyFromIndex, newBuffer, 0, copyBytes);
                ReleaseBufferToPool(ref buffer);
            }

            buffer = newBuffer;
        }
        internal static void ReleaseBufferToPool(ref byte[] buffer)
        {
            if (buffer == null) return;

            lock (Pool)
            {
                var minIndex = 0;
                var minSize = int.MaxValue;
                for (var i = 0; i < Pool.Length; i++)
                {
                    var tmp = Pool[i];
                    if (tmp == null || !tmp.IsAlive)
                    {
                        minIndex = 0;
                        break;
                    }
                    if (tmp.Size < minSize)
                    {
                        minIndex = i;
                        minSize = tmp.Size;
                    }
                }

                Pool[minIndex] = new CachedBuffer(buffer);
            }

            buffer = null;
        }

        private class CachedBuffer
        {
            private readonly WeakReference _reference;

            public int Size { get; }

            public bool IsAlive => _reference.IsAlive;
            public byte[] Buffer => (byte[])_reference.Target;

            public CachedBuffer(byte[] buffer)
            {
                Size = buffer.Length;
                _reference = new WeakReference(buffer);
            }
        }
    }


    //public static class BufferPool
    //{
    //    internal const int BUFFER_LENGTH = 1024;

    //    internal static void Flush()
    //    {
    //        StaticBufferPool.Flush();
    //    }

    //    internal static byte[] GetBuffer()
    //    {
    //        return StaticBufferPool.GetBuffer();
    //    }
    //    internal static byte[] GetBuffer(int minSize)
    //    {
    //        return StaticBufferPool.GetBuffer(minSize);
    //    }
    //    internal static byte[] GetCachedBuffer(int minSize)
    //    {
    //        return StaticBufferPool.GetCachedBuffer(minSize);
    //    }

    //    internal static void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
    //    {
    //        StaticBufferPool.ResizeAndFlushLeft(ref buffer, toFitAtLeastBytes, copyFromIndex, copyBytes);
    //    }
    //    internal static void ReleaseBufferToPool(ref byte[] buffer)
    //    {
    //        StaticBufferPool.ReleaseBufferToPool(ref buffer);
    //    }
    //}


    public interface IBufferPool
    {
        void Flush();
        byte[] GetBuffer();
        byte[] GetBuffer(int minSize);
        byte[] GetCachedBuffer(int minSize);
        void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes);
        void ReleaseBufferToPool(ref byte[] buffer);
    }

    class StaticBufferPoolWrapper : IBufferPool
    {
        public void Flush()
        {
            StaticBufferPool.Flush();
        }

        public byte[] GetBuffer()
        {
            return StaticBufferPool.GetBuffer();
        }
        public byte[] GetBuffer(int minSize)
        {
            return StaticBufferPool.GetBuffer(minSize);
        }
        public byte[] GetCachedBuffer(int minSize)
        {
            return StaticBufferPool.GetCachedBuffer(minSize);
        }

        public void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
        {
            StaticBufferPool.ResizeAndFlushLeft(ref buffer, toFitAtLeastBytes, copyFromIndex, copyBytes);
        }
        public void ReleaseBufferToPool(ref byte[] buffer)
        {
            StaticBufferPool.ReleaseBufferToPool(ref buffer);
        }
    }

    public sealed class InstanceBufferPool : IBufferPool
    {
        private const int PoolSize = 20;
        private const int BufferLength = 1024;

        private readonly CachedBuffer[] pool = new CachedBuffer[PoolSize];

        public void Flush()
        {
            Debug.WriteLine($"[InstanceBufferPool] - Flush");
            for (var i = 0; i < pool.Length; i++)
                pool[i] = null;
        }

        public byte[] GetBuffer() => GetBuffer(BufferLength);
        public byte[] GetBuffer(int minSize)
        {
            byte[] cachedBuff = GetCachedBuffer(minSize);
            return cachedBuff ?? new byte[minSize];
        }
        public byte[] GetCachedBuffer(int minSize)
        {
            var bestIndex = -1;
            byte[] bestMatch = null;
            for (var i = 0; i < pool.Length; i++)
            {
                var buffer = pool[i];
                if (buffer == null || buffer.Size < minSize)
                {
                    continue;
                }
                if (bestMatch != null && bestMatch.Length < buffer.Size)
                {
                    continue;
                }

                var tmp = buffer.Buffer;
                if (tmp == null)
                {
                    pool[i] = null;
                }
                else
                {
                    bestMatch = tmp;
                    bestIndex = i;
                }
            }

            if (bestIndex >= 0)
            {
                pool[bestIndex] = null;
            }

            return bestMatch;
        }

        /// <remarks>
        /// https://docs.microsoft.com/en-us/dotnet/framework/configure-apps/file-schema/runtime/gcallowverylargeobjects-element
        /// </remarks>
        private const int MaxByteArraySize = int.MaxValue - 56;

        public void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
        {
            Helpers.DebugAssert(buffer != null);
            Helpers.DebugAssert(toFitAtLeastBytes > buffer.Length);
            Helpers.DebugAssert(copyFromIndex >= 0);
            Helpers.DebugAssert(copyBytes >= 0);

            int newLength = buffer.Length * 2;
            if (newLength < 0)
                newLength = MaxByteArraySize;

            if (newLength < toFitAtLeastBytes) newLength = toFitAtLeastBytes;

            if (copyBytes == 0)
                ReleaseBufferToPool(ref buffer);

            var newBuffer = GetCachedBuffer(toFitAtLeastBytes) ?? new byte[newLength];

            if (copyBytes > 0)
            {
                Buffer.BlockCopy(buffer, copyFromIndex, newBuffer, 0, copyBytes);
                ReleaseBufferToPool(ref buffer);
            }

            buffer = newBuffer;
        }
        public void ReleaseBufferToPool(ref byte[] buffer)
        {
            if (buffer == null) return;

            var minIndex = 0;
            var minSize = int.MaxValue;
            for (var i = 0; i < pool.Length; i++)
            {
                var tmp = pool[i];
                if (tmp == null || !tmp.IsAlive)
                {
                    minIndex = 0;
                    break;
                }
                if (tmp.Size < minSize)
                {
                    minIndex = i;
                    minSize = tmp.Size;
                }
            }

            pool[minIndex] = new CachedBuffer(buffer);

            buffer = null;
        }

        private class CachedBuffer
        {
            private readonly WeakReference _reference;

            public int Size { get; }

            public bool IsAlive => _reference.IsAlive;
            public byte[] Buffer => (byte[])_reference.Target;

            public CachedBuffer(byte[] buffer)
            {
                Size = buffer.Length;
                _reference = new WeakReference(buffer);
            }
        }
    }

    public class FixedInstanceBufferPool :IBufferPool
    {
        private readonly int bufferSize;
        private readonly Stack<byte[]> pool;

        public FixedInstanceBufferPool(int poolSize, int bufferSize)
        {
            this.bufferSize = bufferSize;
            pool = new Stack<byte[]>(poolSize);
            for(int i = 0; i < poolSize; i++)
                pool.Push(new byte[bufferSize]);
        }

        public void Flush()
        {
        }

        public byte[] GetBuffer() => GetBuffer(bufferSize);
        public byte[] GetBuffer(int minSize) => GetCachedBuffer(minSize);
        public byte[] GetCachedBuffer(int minSize)
        {
            if(minSize > bufferSize)
                return new byte[minSize];

            var cachedBuffer = pool.Pop();
            if(cachedBuffer == null)
                Debug.WriteLine($"[FixedInstanceBufferPool] - GetCachedBuffer: null buffer");
            return cachedBuffer;
        }

        private const int MaxByteArraySize = int.MaxValue - 56;
        public void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
        {
            Helpers.DebugAssert(buffer != null);
            Helpers.DebugAssert(toFitAtLeastBytes > buffer.Length);
            Helpers.DebugAssert(copyFromIndex >= 0);
            Helpers.DebugAssert(copyBytes >= 0);

            int newLength = buffer.Length * 2;
            if (newLength < 0)
                newLength = MaxByteArraySize;

            if (newLength < toFitAtLeastBytes) newLength = toFitAtLeastBytes;

            if (copyBytes == 0)
                ReleaseBufferToPool(ref buffer);

            var newBuffer = GetCachedBuffer(toFitAtLeastBytes) ?? new byte[newLength];

            if (copyBytes > 0)
            {
                Buffer.BlockCopy(buffer, copyFromIndex, newBuffer, 0, copyBytes);
                ReleaseBufferToPool(ref buffer);
            }

            buffer = newBuffer;
        }
        public void ReleaseBufferToPool(ref byte[] buffer)
        {
            if(buffer == null)
                return;
            pool.Push(buffer);
            buffer = null;
        }
    }
    public class LoggingBufferPool : IBufferPool
    {
        private readonly IBufferPool inner;
        public LoggingBufferPool(IBufferPool inner)
        {
            this.inner = inner;
        }

        public void Flush()
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - Flush");
            inner.Flush();
        }

        public byte[] GetBuffer()
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - GetBuffer");
            return inner.GetBuffer();
        }

        public byte[] GetBuffer(int minSize)
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - GetBuffer");
            return inner.GetBuffer(minSize);
        }

        public byte[] GetCachedBuffer(int minSize)
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - GetCachedBuffer");
            return inner.GetCachedBuffer(minSize);
        }

        public void ResizeAndFlushLeft(ref byte[] buffer, int toFitAtLeastBytes, int copyFromIndex, int copyBytes)
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - ResizeAndFlushLeft");
            inner.ResizeAndFlushLeft(ref buffer, toFitAtLeastBytes, copyFromIndex, copyBytes);
        }

        public void ReleaseBufferToPool(ref byte[] buffer)
        {
            Debug.WriteLine($"[{inner.GetType().Name}] - ReleaseBufferToPool");
            inner.ReleaseBufferToPool(ref buffer);
        }
    }
}
